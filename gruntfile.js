module.exports = function (grunt) {
    grunt.initConfig({


        // Watch task config
        watch: {
            sass: {
                files: ['assets/scss/*.scss', 'assets/scss/components/*.scss', 'assets/scss/components/mixins/*.scss'],
                tasks: ['sass']
            },
            styles: {
                files: ['assets/css/flow.css'],
                tasks: ['postcss']
            },
        },


        // SASS task config
        sass: {
            options: {
                //sourceMap: true
            },
            dist: {
                files: {
                    'assets/css/flow.css': 'assets/scss/menu.scss'
                }
            }
        },


        // Autoprefixer
        postcss: {
            options: {
                map: true,

                processors: [
                    require('pixrem')(), // add fallbacks for rem units
                    require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: 'assets/css/flow.css',
                dest: 'assets/css/flow.min.css'
            }
        },


        // Browser-sync for static .html files.
        browserSync: {
            default_options: {
                bsFiles: {
                    src: [
                        "assets/css/flow.min.css",
                        "*.html"
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-postcss');

    // Launch BrowserSync + watch task
    grunt.registerTask('default', ['browserSync', 'watch', 'postcss']);
};